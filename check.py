#!/usr/bin/env python3
from linter import *
import os, sys

def lint(path):
    if path.endswith('.sqf'):
        return check_sqf_syntax(path)
    if path.endswith('.hpp') or path.endswith('.cpp'):
        return check_config_style(path)
    if path.endswith('.json'):
        try: json.loads(open(path, 'r').read())
        except: return 1
    return 0

def lint_recurse(paths):
    err_count = 0
    file_count = 0

    print("Linting code...")

    for basepath in paths:
        for root, dirs, files in os.walk(basepath):
            for file in files:
                fullpath = os.path.join(root, file)
                file_count += 1
                ret = lint(fullpath)
                if ret > 0:
                    print("Error while linting %s" % fullpath)
                    err_count += ret

    print("There were %d error(s) in %d file(s)" % (err_count, file_count))
    return err_count

def lint_all():
    return lint_recurse(["Code", "Islands", "Missions", "Mods"])

if __name__=="__main__":
    sys.exit(lint_all())
