if(!isserver) exitwith {};

private ["_hpPosition", "_createHPcount", "_newPosition"];

_hpPosition = [];
_createHPcount = 0;

if(isNil("A3E_HeliportCount")) then {
        A3E_HeliportCount = ((floor random 3)+(A3E_Param_EnemyFrequency) + 2);
    };

while {_createHPcount < A3E_HeliportCount} do
    {
    _createHPcount = (_createHPcount + 1);
    _newPosition = [50, 1000, 0.1] call A3E_fnc_findFlatArea;
    _hpPosition pushBack _newPosition;
    };

_playergroup = [] call A3E_fnc_getPlayerGroup;
{
    // Fixme: hard coding to 180° orientation for now
    [_x, 180, a3e_arr_ComCenStaticWeapons,
     a3e_arr_Escape_MilitaryTraffic_CivilianVehicleClasses,
	 a3e_arr_O_transport_heli]
     call A3E_fnc_BuildHeliport;
     //a3e_arr_Escape_heliClasses

    [_playergroup, "A3E_HeliportPatrolMarker", A3E_VAR_Side_Opfor, "INS", 5, 5, 8,
     A3E_Param_EnemySkill, A3E_Param_EnemySkill, A3E_Param_EnemySpawnDistance, false] spawn drn_fnc_InitGuardedLocations;
} foreach _hpPosition;
