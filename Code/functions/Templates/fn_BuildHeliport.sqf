private ["_centerPos", "_rotateDir", "_staticWeaponClasses", "_parkedVehicleClasses", "_index"];
private ["_pos", "_dir", "_posX", "_posY", "_sectionDir", "_guns", "_gun", "_vehicle", "_powerGenerator","_static"];
private ["_fnc_CreateObject", "_parkedArmorClasses", "_helicopter", "_random", "_objectName", "_hqObject"];
private ["_obj", "_mNumber", "_sheli", "_objpos", "_guardtower", "_staticAAClasses", "_newpos", "_aaemplacement", "_staticAA"];

if(isNil("A3E_HeliportMarkerNumber")) then {
    A3E_HeliportMarkerNumber = 0;
} else {
    A3E_HeliportMarkerNumber =A3E_HeliportMarkerNumber +1;
};

_mNumber = A3E_HeliportMarkerNumber;

_centerPos            = [_this, 0] call bis_fnc_param;
_rotateDir            = [_this, 1] call bis_fnc_param;
_staticWeaponClasses  = [_this, 2, []] call bis_fnc_param;
_parkedVehicleClasses = [_this, 3, []] call bis_fnc_param;
_heliClasses          = [_this, 4, []] call bis_fnc_param;
_staticAAClasses      = [_this, 5, []] call bis_fnc_param;


_fnc_CreateObject = {
    private ["_className", "_relativePos", "_relativeDir", "_centerPos", "_rotateDir"];
    private ["_object", "_realPos", "_realDir", "_alignWithSurface", "_normal"];
    
    _className = _this select 0;
    _relativePos = _this select 1;
    _relativeDir = _this select 2;
    _centerPos = _this select 3;
    _rotateDir = _this select 4;
    if (count _this > 5) then {
        _alignWithSurface = _this select 5;
    } else {
        _alignWithSurface = false;
    };
    // Object Placement script
    _realPos = ([_centerPos, [(_centerPos select 0) + (_relativePos select 0), (_centerPos select 1) + (_relativePos select 1)], _rotateDir] call a3e_fnc_RotatePosition);
    if (count _relativePos > 2) then {_realPos set [2, (_relativePos select 2)]};	
    _realDir = _relativeDir + _rotateDir;
    _object = createVehicle [_className, _realPos, [], 0, "CAN_COLLIDE"]; 
    if (_className == "Land_Cargo_HQ_V1_F" && (count _staticAAClasses > 0)) then {
        _newpos = [(_realPos select 0), (_realPos select 1), 3];
        _staticAA = _staticAAClasses call BIS_fnc_selectRandom;
        _aaemplacement = createVehicle [_staticAA, _newpos, [], 0, "CAN_COLLIDE"];
        _aaemplacement setDir (_realDir + 180);
        _aaemplacement attachTo [_object,[4.7,1.3,1.2]];
        [_aaemplacement,east] spawn A3E_fnc_AddStaticGunner; 
    };
    // Object Facing and Vector Up script
    _object setDir _realDir;
    if (_alignWithSurface) then {
        _normal = (surfaceNormal _realPos);
        _object setVectorUp _normal;
    };
    _object
};

_fnc_CreateVehicle = {
    private ["_className", "_relativePos", "_relativeDir", "_centerPos", "_rotateDir"];
    private ["_object", "_realPos", "_realDir"];
    
    _className = _this select 0;
    _relativePos = _this select 1;
    _relativeDir = _this select 2;
    _centerPos = _this select 3;
    _rotateDir = _this select 4;
    
    _realPos = ([_centerPos, [(_centerPos select 0) + (_relativePos select 0), (_centerPos select 1) + (_relativePos select 1)], _rotateDir] call a3e_fnc_RotatePosition);
    _realDir = _relativeDir + _rotateDir;
    _object = createVehicle [_className, _realpos, [], 0, "NONE"];
    _object setDir _realDir;
    _object
};

_fnc_InitHeli = {
    _this setDamage (random 0.33);
    _this setFuel (random 0.03 + 0.01);

    clearMagazineCargoGlobal _this;
    clearBackpackCargoGlobal _this;
    clearWeaponCargoGlobal _this;
    clearItemCargoGlobal _this;
    _this addBackpackCargoGlobal ["B_Parachute", 6];
};

//// Objects!
/// Heavy Barriers
// East Wall
_pos = [26.957, -10.414];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [26.957, -1.918];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [26.968, 6.562];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [26.963, 15.039];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [26.957, -18.912];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

// West Wall
_pos = [-27.174, -19.079];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [-27.174, -10.58];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [-27.174, -2.084];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [-27.162, 6.396];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [-27.168, 14.873];
_dir = 90;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

// North Wall
_pos = [-17.022, 24.435];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [-8.58, 24.423];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [-0.0550000000003, 24.425];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [8.437, 24.42];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [16.925, 24.423];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

// South Wall
_pos = [-7.258, -24.495];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [-15.707, -24.495];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [-24.209, -24.487];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [15.47, -24.389];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [24.011, -24.387];
_dir = 180;
["Land_HBarrierBig_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

// Some signs
_pos = [-1.873, -26.09];
_dir = 0;
["Land_Sign_WarningMilitaryVehicles_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

_pos = [9.957, -26.012];
_dir = 0;
["Land_Sign_WarningMilitaryVehicles_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

// SW Objects
_pos = [-21.912, -7.346];
_dir = 105;
["Land_PowerGenerator_F", _pos, _dir, _centerPos, _rotateDir, true] call _fnc_CreateObject;

/// Southwest Corner Structures
_pos = [-16.176, -13.577];
_dir = 90;
["Land_Radar_Small_F", _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateObject;

/// Northwest Corner Structures
_pos = [-18.844, 22.31];
_dir = 0;
["Land_FieldToilet_F", _pos, _dir, _centerPos, _rotateDir, true] call _fnc_CreateObject;

_pos = [-20.233, 22.31];
_dir = 0;
["Land_FieldToilet_F", _pos, _dir, _centerPos, _rotateDir, true] call _fnc_CreateObject;

_pos = [-24.735, 12.254];
_dir = 90;
["Land_WaterTank_F", _pos, _dir, _centerPos, _rotateDir, true] call _fnc_CreateObject;

_pos = [-24.733, 8.341];
_dir = 90;
["Land_LampStreet_F", _pos, _dir, _centerPos, _rotateDir, true] call _fnc_CreateObject;

/// Northeast Corner Structures
_pos = [6.0, 5.0];
_dir = 0;
["HeliH", _pos, _dir, _centerPos, _rotateDir, true] call _fnc_CreateObject;

_pos = [-10.0, 5.0];
_dir = 0;
["HeliH", _pos, _dir, _centerPos, _rotateDir, true] call _fnc_CreateObject;


// Helicopter
if (count _heliClasses > 0) then {
    _pos = [6.0, 5.0];
    _dir = 180;
    _helicopter = _heliClasses select floor random count _heliClasses;

    _sheli = [_helicopter, _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateVehicle;
    _sheli call _fnc_InitHeli;

    // Random second heli
    if (random 100 < 60) then {
        _pos = [-10.0, 5.0];
        _helicopter = _heliClasses select floor random count _heliClasses;
        _sheli = [_helicopter, _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateVehicle;
        _sheli call _fnc_InitHeli;
    };
};
// setVehicleAmmo cannot be used until Ammo Depots rearm all vehicles


// Vehicles
if (count _parkedVehicleClasses > 0) then {
    // Cars
    _pos = [11.136, 7.155];
    _dir = 180;
    
    _vehicle = _parkedVehicleClasses select floor random count _parkedVehicleClasses;
    [_vehicle, _pos, _dir, _centerPos, _rotateDir] call _fnc_CreateVehicle;
        
};

["A3E_HeliportMapMarker" + str _mNumber,_centerPos,"o_service"] call A3E_fnc_createLocationMarker;

_marker = createMarkerLocal ["A3E_HeliportPatrolMarker" + str _mNumber, _centerPos];
_marker setMarkerShapeLocal "RECTANGLE";
_marker setMarkerAlphaLocal 0;
_marker setMarkerSizeLocal [40, 38];

