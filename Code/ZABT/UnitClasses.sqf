/*
 * This file contains all special items in addition to the normal modded ones that the ACE3 system
 * provides.  This file is only executed when the Param_ACE == 1
 */

if (isNil("a3e_arr_PrisonBackpackItems")) then {
	a3e_arr_PrisonBackpackItems = [];
};

//a3e_arr_PrisonBackpackItems pushback ["ACE_wirecutter"];
a3e_arr_PrisonBackpackItems pushback ["ACE_EntrenchingTool"];
a3e_arr_PrisonBackpackItems pushback ["ACE_Flashlight_XL50"];
//a3e_arr_PrisonBackpackItems pushback ["ACE_Banana"];
a3e_arr_PrisonBackpackItems pushback ["ACE_HandFlare_Red"];
a3e_arr_PrisonBackpackItems pushback ["ACE_HandFlare_Green"];

//a3e_arr_PrisonBackpackWeapons pushback ["hgun_Pistol_Signal_F","6Rnd_GreenSignal_F"];

// Ammo depot items; defined in mod UnitClasses.sqf

a3e_arr_AmmoDepotItems pushback ["ACE_Sandbag_empty", 50, 15, 50];
a3e_arr_AmmoDepotItems pushback ["ACE_EntrenchingTool", 50, 1, 2];
a3e_arr_AmmoDepotItems pushback ["ACE_wirecutter", 50, 1, 4];
a3e_arr_AmmoDepotItems pushback ["ACE_Flashlight_MX991Fulton", 50, 1, 2];
a3e_arr_AmmoDepotItems pushback ["ACE_Flashlight_XL50", 50, 1, 2];
a3e_arr_AmmoDepotItems pushback ["ACE_Flashlight_KSF1", 50, 1, 2];
a3e_arr_AmmoDepotItems pushback ["ACE_EarPlugs", 50, 1, 4];
a3e_arr_AmmoDepotItems pushback ["ACE_Banana", 50, 5, 10];
a3e_arr_AmmoDepotItems pushback ["ACE_SpottingScope", 50, 1, 1];
a3e_arr_AmmoDepotItems pushback ["ACE_TacticalLadder_Pack", 25, 1, 2];
a3e_arr_AmmoDepotItems pushback ["ACE_Tripod", 20, 1, 2];

//a3e_arr_AmmoDepotBasicWeapons pushback ["hgun_Pistol_Signal_F", 70, 4, 8, ["6Rnd_GreenSignal_F", "6Rnd_RedSignal_F"], 6];

a3e_arr_AmmoDepotVehicle pushback [objNull, 50, 1, 1, ["ACE_HandFlare_White", "ACE_HandFlare_Red", "ACE_HandFlare_Green", "ACE_HandFlare_Yellow"], 50];
a3e_arr_AmmoDepotVehicle pushback [objNull, 50, 1, 1, ["ACE_M84"], 50];

if(A3E_Param_NoNightvision==0) then {
	a3e_arr_AmmoDepotItems pushback ["ACE_NVG_Wide", 10, 1, 3];
	a3e_arr_AmmoDepotItems pushback ["ACE_NVG_Gen4", 10, 1, 3];
};

a3e_arr_AmmoDepotVehicle pushback [objNull, 90, 2, 3, ["ACE_HuntIR_M203"], 2];
a3e_arr_AmmoDepotVehicleItems pushback ["ACE_HuntIR_monitor", 90, 1, 2, [], 0];

// Add things to patrol scopes and such

a3e_arr_Scopes pushback "ACE_optic_Arco_2D";
a3e_arr_Scopes pushback "ACE_optic_LRPS_2D";
a3e_arr_Scopes pushback "ACE_optic_SOS_2D";
a3e_arr_Scopes pushback "ACE_optic_MRCO_2D";
a3e_arr_Scopes pushback "ACE_optic_Hamr_2D";

// Add random items to soliders
// ["possible_item", ...], count, likelihood

if (isNil("a3e_arr_GeneralSoliderRandomItems")) then {
	a3e_arr_GeneralSoliderRandomItems = [];
};

a3e_arr_GeneralSoliderRandomItems pushback [["ACE_HandFlare_White", "ACE_HandFlare_Red", "ACE_HandFlare_Green", "ACE_HandFlare_Yellow"], 3, 50];
a3e_arr_GeneralSoliderRandomItems pushback [["ACE_M84"], 1, 30];
a3e_arr_GeneralSoliderRandomItems pushBack [["ACE_MX2A"], 1, 15];
a3e_arr_GeneralSoliderRandomItems pushBack [["ACE_SpottingScope","ACE_Kestrel4500","ACE_EntrenchingTool","ACE_wirecutter","ACE_Flashlight_XL50"], 1, 10];

// Add items to cars
a3e_arr_CivilianCarWeapons pushback [objNull, "ACE_HandFlare_White", 5];
a3e_arr_CivilianCarWeapons pushback [objNull, "ACE_HandFlare_Green", 5];
a3e_arr_CivilianCarWeapons pushback ["hgun_Pistol_Signal_F", "6Rnd_RedSignal_F", 3];
