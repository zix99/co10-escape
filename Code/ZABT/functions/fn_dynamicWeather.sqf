if (!isServer) exitWith {};

// How often to change weather
private _sleep = 60*60*4/A3E_Param_TimeMultiplier;
private _randomness = 60*30/A3E_Param_TimeMultiplier;

// How many real-seconds it takes to tween the weather;
private _tween = 60;



// Start weather system...
[] spawn ZABT_fnc_weather;
forceWeatherChange; // Immediate

while {true} do {
	sleep (_sleep + random _randomness);
	[_tween] spawn ZABT_fnc_weather;
}
