ZABT_TerminalTypes = ["I_UavTerminal", "C_UavTerminal", "O_UavTerminal", "I_E_UavTerminal", "B_UavTerminal"];
publicVariable 'ZABT_TerminalTypes';

fnc_hackUavTerminal = {
    params ["_target", "_caller", "_actionId", "_arguments"];
    
    private _terminalClass = "";
    switch (side player) do {
        case WEST: {_terminalClass = "B_UavTerminal";};
        case EAST: {_terminalClass = "O_UavTerminal";};
        case INDEPENDENT: {_terminalClass = "I_UavTerminal";};
    };

    // Remove existing terminal
    player unlinkItem "ItemGPS";
    player removeItem "ItemGPS";

    // Add new terminal
    player addItem _terminalClass;
    player assignItem _terminalClass;

    player setVariable ["ZABT_hasUav", false]; // Hide menu item
};

fnc_evalCanHackAction = {
    params ["_unit", "_container", "_item"];
    player setVariable ['ZABT_hasUav', false];

    //_roles = ["I_soldier_UAV_F", "O_soldier_UAV_F", "B_soldier_UAV_F"];
    //if (typeOf _unit in _roles) then {
        private _items = (items _unit + assignedItems _unit);
        private _uavTerm = _items findIf { _x in ZABT_TerminalTypes };

        private _terminalClass = "";
        switch (side _unit) do {
            case WEST: {_terminalClass = "B_UavTerminal";};
            case EAST: {_terminalClass = "O_UavTerminal";};
            case INDEPENDENT: {_terminalClass = "I_UavTerminal";};
        };

        if ( _uavTerm >= 0 ) then {
            player setVariable ['ZABT_hasUav', (_items select _uavTerm != _terminalClass)];
        };
   // };
};

// Initial evaluate
[player, nil, nil] call fnc_evalCanHackAction;

// EH's
player addEventHandler ["Put",  fnc_evalCanHackAction];
player addEventHandler ["Take", fnc_evalCanHackAction];

// Action (Only show if player variable set)
player addAction ["Hack UAV Terminal", "0 call fnc_hackUavTerminal", [], 1.5, false, true, "", "player getVariable 'ZABT_hasUav'"];
