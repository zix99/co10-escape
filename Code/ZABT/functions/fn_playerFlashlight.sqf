
player setVariable ["lightIsOn", false];

fnc_turnLightOn = {
	playerLight = "#lightpoint" createVehicle position _this;
	playerLight attachTo [_this, [0,4.7,1]];
	[playerLight, {
		_this setLightBrightness 0.3;  
		_this setLightAmbient[ 0.1, 0.1, 0.1];
		_this setLightColor [1, 0.5, 0.5];
		_this setLightDayLight false;
	}] remoteExecCall ["bis_fnc_call", 0];
};

fnc_turnLightOff = {
	deleteVehicle playerLight;
};

fnc_togglePlayerLight = {
	if (player getVariable "lightIsOn") then {
		player call fnc_turnLightOff;
		player setVariable ["lightIsOn", false];
	} else {
		player call fnc_turnLightOn;
		player setVariable ["lightIsOn", true];
	};
};

player addAction ["<t color=""#ccaa00"">Toggle Flashlight</t>", "[] call fnc_togglePlayerLight", nil, 0, false];
